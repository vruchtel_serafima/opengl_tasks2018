#include "Pseudosphere.hpp"

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <glm/gtc/reciprocal.hpp> 

#include <iostream>
#include <vector>


MeshPtr makePseudosphere(float size, unsigned int N)
{
    int M = N / 2;

	auto coords = [](float u, float v) {
		float x = glm::sech(u) * cos(v);
		float y = glm::sech(u) * sin(v);
		float z = u - tanh(u);
		
		return glm::vec3(x, y, z);
	};	
	
	auto vectors_product = [](glm::vec3 v1, glm::vec3 v2) {
		return glm::vec3(v1[1] * v2[2] - v1[2] * v2[1], v1[2] * v2[0] - v1[0] * v2[2], v1[0] * v2[1] - v1[1] * v2[0]);
	};

	auto calculate_normal = [coords, vectors_product](float u, float v) {
		float step = 1e-5;
		auto coords_in_point = coords(u, v);
		auto v1 = coords(u + step, v) - coords_in_point;
		auto v2 = coords(u, v + step) - coords_in_point;
		auto n = vectors_product(v1, v2);
		float normal_length = sqrt(n[0] * n[0] + n[1] * n[1] + n[2] * n[2]);
		return -glm::vec3(n[0] / normal_length, n[1] / normal_length, n[2] / normal_length);
	};
 
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    for (int i = -M; i < M; i++)
    {
        float theta = (float)glm::pi<float>() * i / M;
        float theta1 = (float)glm::pi<float>() * (i + 1) / M;

        for (int j = 0; j < N; j++)
        {
            float phi = 2.0f * (float)glm::pi<float>() * j / N + (float)glm::pi<float>();
            float phi1 = 2.0f * (float)glm::pi<float>() * (j + 1) / N + (float)glm::pi<float>();
            
            auto vertice_00 = coords(theta, phi) * size;
            auto vertice_01 = coords(theta, phi1) * size;
            auto vertice_10 = coords(theta1, phi) * size;
            auto vertice_11 = coords(theta1, phi1) * size;
            
            auto normal_00 = calculate_normal(theta, phi);
            auto normal_01 = calculate_normal(theta, phi1);
			auto normal_10 = calculate_normal(theta1, phi);
			auto normal_11 = calculate_normal(theta1, phi1);

            //Первый треугольник, образующий квад            
            vertices.push_back(vertice_00);
            vertices.push_back(vertice_01);
            vertices.push_back(vertice_10);         
            
            normals.push_back(normal_00);
            normals.push_back(normal_01);
			normals.push_back(normal_10);

            texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)i / M));
            texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)(i + 1) / M));
            texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)i / M));

            //Второй треугольник, образующий квад
            vertices.push_back(vertice_01);
            vertices.push_back(vertice_10);
			vertices.push_back(vertice_11);
            
            normals.push_back(normal_01);
            normals.push_back(normal_10);
            normals.push_back(normal_11);

            texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)i / M));
            texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)(i + 1) / M));
            texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)(i + 1) / M));
        }
    }

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    std::cout << "Pseudosphere is created with " << vertices.size() << " vertices\n";

    return mesh;
}
