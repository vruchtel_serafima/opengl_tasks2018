#include <Mesh.hpp>

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <glm/gtc/reciprocal.hpp> 

#include <iostream>
#include <vector>
#include <sstream>

#include <Application.hpp>
#include <LightInfo.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>

#include "Pseudosphere.hpp"


class SampleApplication : public Application
{
public:
    MeshPtr _pseudosphere;
    MeshPtr _marker; //Меш - маркер для источника света

    //Идентификатор шейдерной программы
    ShaderProgramPtr _commonShader;
    ShaderProgramPtr _markerShader;

    //Переменные для управления положением одного источника света
    float _lr = 10.0f;
    float _phi = 0.0f;
    float _theta = 0.48f;
    int _polygons = 250;
    float _roundPhi = 0.0f;
    glm::vec3 _roundColor = glm::vec3(1.0f, 0.0f, 0.0f);
    
    int _last_polygons = _polygons;

    LightInfo _light;

    TexturePtr _worldTex;
    TexturePtr _brickTex;
    TexturePtr _grassTex;

    GLuint _sampler;

    void makeScene() override
    {
        Application::makeScene();

        //=========================================================
        //Создание и загрузка мешей

        _pseudosphere = makePseudosphere(0.5, _polygons);
        _pseudosphere->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));

        _marker = makeSphere(0.1f);

        //=========================================================
        //Инициализация шейдеров

        _commonShader = std::make_shared<ShaderProgram>("495vruchtelData/common.vert", "495vruchtelData/common.frag");
        _markerShader = std::make_shared<ShaderProgram>("495vruchtelData/marker.vert", "495vruchtelData/marker.frag");

        //=========================================================
        //Инициализация значений переменных освщения
        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(1.0, 1.0, 1.0);

        //=========================================================
        //Загрузка и создание текстур
        _worldTex = loadTexture("495vruchtelData/earth_global.jpg");
        _brickTex = loadTexture("495vruchtelData/brick.jpg");
        _grassTex = loadTexture("495vruchtelData/grass.jpg");

        //=========================================================
        //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Light"))
            {
                ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
                ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

                ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
            }
            if (ImGui::CollapsingHeader("Detalization"))
            {
				ImGui::SliderInt("polygons", &_polygons, 1, 1000);
				if (_polygons != _last_polygons) {
					_pseudosphere = makePseudosphere(0.5, _polygons);
					_pseudosphere->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));
					_last_polygons = _polygons;
				}
			}
        }
        ImGui::End();
    }

    void draw() override
    {
        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

        //====== РИСУЕМ ОСНОВНЫЕ ОБЪЕКТЫ СЦЕНЫ ======
        _commonShader->use();

        //Загружаем на видеокарту значения юниформ-переменных
        _commonShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _commonShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));
        
        _roundPhi += 0.002;
        glm::vec3 roundPos = glm::vec3(glm::cos(_roundPhi), glm::sin(_roundPhi), 0.0f) * 1.0f;
        glm::vec3 roundPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(roundPos, 1.0));

        _commonShader->setVec3Uniform("light.La", _light.ambient);
        // 1й источник света
        _commonShader->setVec3Uniform("light.roundPos", roundPosCamSpace); //копируем положение уже в системе виртуальной камеры
        _commonShader->setVec3Uniform("light.roundLD", glm::vec3(1.0f, 1.0f, 0.0f));
        _commonShader->setVec3Uniform("light.roundLS", glm::vec3(1.0f, 1.0f, 0.0f));
        // 2й источник света
        _commonShader->setVec3Uniform("light.headLD", glm::vec3(0.0f, 0.0f, 1.0f));
        _commonShader->setVec3Uniform("light.headLS", glm::vec3(0.0f, 0.0f, 1.0f));
        // 3й источник света
        _commonShader->setVec3Uniform("light.thirdPos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
        _commonShader->setVec3Uniform("light.thirdLD", _light.diffuse);
        _commonShader->setVec3Uniform("light.thirdLS", _light.specular);

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _sampler);
        _worldTex->bind(); // здесь связывается текстура с моей фигурой
        _commonShader->setIntUniform("diffuseTex", 0);

        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        {
			if (_polygons != _last_polygons) {
				_pseudosphere = makePseudosphere(0.5, _polygons);
				_pseudosphere->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));
				_last_polygons = _polygons;
			}
			
            _commonShader->setMat4Uniform("modelMatrix", _pseudosphere->modelMatrix());
            _commonShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _pseudosphere->modelMatrix()))));

            _pseudosphere->draw();
        }

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _sampler);
        _brickTex->bind();
        _commonShader->setIntUniform("diffuseTex", 0);

        glEnable(GL_STENCIL_TEST);

        glStencilFunc(GL_ALWAYS, 1, 1);
        glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

        glDisable(GL_STENCIL_TEST);

		//Рисуем маркеры для всех источников света
        {
            _markerShader->use();
            _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), _light.position));
            _markerShader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
            _marker->draw();
            
             _markerShader->use();
            _markerShader->setMat4Uniform("mvpMatrix", _camera.projMatrix * _camera.viewMatrix * glm::translate(glm::mat4(1.0f), roundPos));
            _markerShader->setVec4Uniform("color", glm::vec4(_light.diffuse, 1.0f));
            _marker->draw();
        }

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);
    }
    
    void handleKey(int key, int scancode, int action, int mods) override {
		Application::handleKey(key, scancode, action, mods);
		
		if (action == GLFW_PRESS) {
			if (key == GLFW_KEY_KP_ADD || key == GLFW_KEY_EQUAL) {
				if (_polygons < 7) {
					_polygons = _polygons + 1;
				}	
				if (_polygons < 25 && _polygons >= 7) {
					_polygons = std::min<int>(25, _polygons + 5);
				}
				else if (_polygons >= 25) {
					_polygons = std::min<int>(1000, _polygons + 25);
				}
				
			}
			if (key == GLFW_KEY_MINUS) {
				if (_polygons < 7) {
					_polygons = std::max<int>(1, _polygons - 1);
				}	
				if (_polygons < 25 && _polygons >= 7) {
					_polygons = std::max<int>(6, _polygons - 5);
				}
				else if (_polygons >= 25) {
					_polygons = std::max<int>(24, _polygons - 25);
				}
			}
		}

		//_cameraMover->handleKey(_window, key, scancode, action, mods);
	};
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}
