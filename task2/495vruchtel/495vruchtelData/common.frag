#version 330

uniform sampler2D diffuseTex;
bool useTexture;

struct LightInfo
{
	vec3 La; //цвет и интенсивность окружающего света
	vec3 roundPos; // положение 1го источника света в системе координат виртуальной камеры
	vec3 roundLD; // цвет и интенсивность диффузного света от 1го источника, движущегося вокруг поверхности
	vec3 roundLS; // цвет и интенсивность бликового света от 1го источника, движущегося вокруг поверхности
	vec3 headLD; // цвет и интенсивность диффузного света от 2го источника - фонарика на голове
	vec3 headLS; // цвет и интенсивность бликового света от 2го источника - фонарика на голове
	vec3 thirdPos; //положение 3го источника света в системе координат ВИРТУАЛЬНОЙ КАМЕРЫ!
	vec3 thirdLD; // цвет и интенсивность диффузного света от 3го источника
	vec3 thirdLS; // цвет и интенсивность бликового света от 3го источника
	
};
uniform LightInfo light;

in vec3 normalCamSpace; //нормаль в системе координат камеры (интерполирована между вершинами треугольника)
in vec4 posCamSpace; //координаты вершины в системе координат камеры (интерполированы между вершинами треугольника)
in vec2 texCoord; //текстурные координаты (интерполирована между вершинами треугольника)

out vec4 fragColor; //выходной цвет фрагмента

const vec3 Ks = vec3(1.0, 1.0, 1.0); //Коэффициент бликового отражения
const float shininess = 128.0;

void main()
{	
	vec3 normal = normalize(normalCamSpace); //нормализуем нормаль после интерполяции
	vec3 diffuseColor = texture(diffuseTex, texCoord).rgb;
	
	vec3 color = diffuseColor * light.La; // компонента цвета, зависящая только от окружающего света
	
	/* 1й источник света - двигается вокруг поверхности */
	{
		vec3 lightDirCamSpace = light.roundPos - posCamSpace.xyz; //направление на источник света (т.к. светит из головы)
		float distance = length(lightDirCamSpace);
		lightDirCamSpace = normalize(lightDirCamSpace); //направление на источник света
	
		float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0); //скалярное произведение (косинус)
	
		color += diffuseColor * light.roundLD * NdotL;
	
		if (NdotL > 0.0) {
			vec3 viewDirection = normalize(-posCamSpace.xyz); //направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))
			vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //биссектриса между направлениями на камеру и на источник света
			float blinnTerm = max(dot(normal, halfVector), 0.0); //интенсивность бликового освещения по Блинну
			blinnTerm = pow(blinnTerm, shininess); //регулируем размер блика
			color += light.roundLS * Ks * blinnTerm;
		}
	}
	
	/* 2й источник света - фонарик на голове */
	{
		vec3 lightDirCamSpace = -posCamSpace.xyz; //направление на источник света (т.к. светит из головы)
		float distance = length(lightDirCamSpace);
		lightDirCamSpace = normalize(lightDirCamSpace); //направление на источник света

		float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0); //скалярное произведение (косинус)
		float radialCoefficient = lightDirCamSpace.z - cos(0.05);
		if (radialCoefficient < 0) {
			radialCoefficient = 0;
		} else {
			radialCoefficient = 1;
		}
	
		color += diffuseColor * light.headLD * NdotL * radialCoefficient;
	
		if (NdotL > 0.0) {
			vec3 viewDirection = normalize(-posCamSpace.xyz); //направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))
			vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //биссектриса между направлениями на камеру и на источник света
			float blinnTerm = max(dot(normal, halfVector), 0.0); //интенсивность бликового освещения по Блинну
			blinnTerm = pow(blinnTerm, shininess); //регулируем размер блика
			color += light.headLS * Ks * blinnTerm * radialCoefficient;
		}
	}
	
	/* 3й источник света */
	{
		vec3 lightDirCamSpace = light.thirdPos - posCamSpace.xyz; //направление на источник света
		float distance = length(lightDirCamSpace);
		lightDirCamSpace = normalize(lightDirCamSpace); //направление на источник света
	
		float NdotL = max(dot(normal, lightDirCamSpace.xyz), 0.0); //скалярное произведение (косинус)
	
		color += diffuseColor * light.thirdLD * NdotL;
	
		if (NdotL > 0.0) {
			vec3 viewDirection = normalize(-posCamSpace.xyz); //направление на виртуальную камеру (она находится в точке (0.0, 0.0, 0.0))
			vec3 halfVector = normalize(lightDirCamSpace.xyz + viewDirection); //биссектриса между направлениями на камеру и на источник света
			float blinnTerm = max(dot(normal, halfVector), 0.0); //интенсивность бликового освещения по Блинну
			blinnTerm = pow(blinnTerm, shininess); //регулируем размер блика
			color += light.thirdLS * Ks * blinnTerm;
		}
	}
    fragColor = vec4(color, 0.5);
}
