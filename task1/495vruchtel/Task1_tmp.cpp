#include <Mesh.hpp>

#include <assimp/cimport.h>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

#include <glm/gtc/reciprocal.hpp> 

#include <iostream>
#include <vector>
#include <sstream>

#include <Application.hpp>
#include <LightInfo.hpp>
#include <ShaderProgram.hpp>
#include <Texture.hpp>

MeshPtr makePseudosphere(float size=1, unsigned int N=1000)
{
    int M = N / 2;

	auto coords = [](float u, float v) {
		float x = glm::sech(u) * cos(v);
		float y = glm::sech(u) * sin(v);
		float z = u - tanh(u);
		
		return glm::vec3(x, y, z);
	};	
	
	auto vectors_product = [](glm::vec3 v1, glm::vec3 v2) {
		return glm::vec3(v1[1] * v2[2] - v1[2] * v2[1], v1[2] * v2[0] - v1[0] * v2[2], v1[0] * v2[1] - v1[1] * v2[0]);
	};

	auto calculate_normal = [coords, vectors_product](float u, float v) {
		float step = 1e-5;
		auto coords_in_point = coords(u, v);
		auto v1 = coords(u + step, v) - coords_in_point;
		auto v2 = coords(u, v + step) - coords_in_point;
		auto n = vectors_product(v1, v2);
		float normal_length = sqrt(n[0] * n[0] + n[1] * n[1] + n[2] * n[2]);
		return glm::vec3(n[0] / normal_length, n[1] / normal_length, n[2] / normal_length);
	};
 
    std::vector<glm::vec3> vertices;
    std::vector<glm::vec3> normals;
    std::vector<glm::vec2> texcoords;

    for (int i = -M; i < M; i++)
    {
        float theta = (float)glm::pi<float>() * i / M;
        float theta1 = (float)glm::pi<float>() * (i + 1) / M;

        for (int j = 0; j < N; j++)
        {
            float phi = 2.0f * (float)glm::pi<float>() * j / N + (float)glm::pi<float>();
            float phi1 = 2.0f * (float)glm::pi<float>() * (j + 1) / N + (float)glm::pi<float>();
            
            auto vertice_00 = coords(theta, phi) * size;
            auto vertice_01 = coords(theta, phi1) * size;
            auto vertice_10 = coords(theta1, phi) * size;
            auto vertice_11 = coords(theta1, phi1) * size;
            
            auto normal_00 = calculate_normal(theta, phi);
            auto normal_01 = calculate_normal(theta, phi1);
			auto normal_10 = calculate_normal(theta1, phi);
			auto normal_11 = calculate_normal(theta1, phi1);

            //Первый треугольник, образующий квад            
            vertices.push_back(vertice_00);
            vertices.push_back(vertice_01);
            vertices.push_back(vertice_10);         
            
            normals.push_back(normal_00);
            normals.push_back(normal_01);
			normals.push_back(normal_10);

            texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)i / M));
            texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)(i + 1) / M));
            texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)i / M));

            //Второй треугольник, образующий квад
            vertices.push_back(vertice_01);
            vertices.push_back(vertice_10);
			vertices.push_back(vertice_11);
            
            normals.push_back(normal_01);
            normals.push_back(normal_10);
            normals.push_back(normal_11);

            texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)i / M));
            texcoords.push_back(glm::vec2((float)j / N, 1.0f - (float)(i + 1) / M));
            texcoords.push_back(glm::vec2((float)(j + 1) / N, 1.0f - (float)(i + 1) / M));
        }
    }

    //----------------------------------------

    DataBufferPtr buf0 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf0->setData(vertices.size() * sizeof(float) * 3, vertices.data());

    DataBufferPtr buf1 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf1->setData(normals.size() * sizeof(float) * 3, normals.data());

    DataBufferPtr buf2 = std::make_shared<DataBuffer>(GL_ARRAY_BUFFER);
    buf2->setData(texcoords.size() * sizeof(float) * 2, texcoords.data());

    MeshPtr mesh = std::make_shared<Mesh>();
    mesh->setAttribute(0, 3, GL_FLOAT, GL_FALSE, 0, 0, buf0);
    mesh->setAttribute(1, 3, GL_FLOAT, GL_FALSE, 0, 0, buf1);
    mesh->setAttribute(2, 2, GL_FLOAT, GL_FALSE, 0, 0, buf2);
    mesh->setPrimitiveType(GL_TRIANGLES);
    mesh->setVertexCount(vertices.size());

    std::cout << "Pseudosphere is created with " << vertices.size() << " vertices\n";

    return mesh;
}


class SampleApplication : public Application
{
public:
    MeshPtr _pseudosphere;

    MeshPtr _marker; //Меш - маркер для источника света

    //Идентификатор шейдерной программы
    ShaderProgramPtr _commonShader;
    ShaderProgramPtr _markerShader;

    //Переменные для управления положением одного источника света
    float _lr = 10.0f;
    float _phi = 0.0f;
    float _theta = 0.48f;
    int _polygons = 250;
    
    int _last_polygons = _polygons;

    LightInfo _light;

    TexturePtr _worldTex;
    TexturePtr _brickTex;
    TexturePtr _grassTex;

    GLuint _sampler;

    void makeScene() override
    {
        Application::makeScene();

        //=========================================================
        //Создание и загрузка мешей

        _pseudosphere = makePseudosphere(0.5, _polygons);
        _pseudosphere->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));

        _marker = makeSphere(0.1f);

        //=========================================================
        //Инициализация шейдеров

        _commonShader = std::make_shared<ShaderProgram>("495vruchtelData/common.vert", "495vruchtelData/common.frag");
        _markerShader = std::make_shared<ShaderProgram>("495vruchtelData/marker.vert", "495vruchtelData/marker.frag");

        //=========================================================
        //Инициализация значений переменных освщения
        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        _light.ambient = glm::vec3(0.2, 0.2, 0.2);
        _light.diffuse = glm::vec3(0.8, 0.8, 0.8);
        _light.specular = glm::vec3(1.0, 1.0, 1.0);

        //=========================================================
        //Загрузка и создание текстур
        _worldTex = loadTexture("495vruchtelData/earth_global.jpg");
        _brickTex = loadTexture("495vruchtelData/brick.jpg");
        _grassTex = loadTexture("495vruchtelData/grass.jpg");

        //=========================================================
        //Инициализация сэмплера, объекта, который хранит параметры чтения из текстуры
        glGenSamplers(1, &_sampler);
        glSamplerParameteri(_sampler, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glSamplerParameteri(_sampler, GL_TEXTURE_WRAP_T, GL_REPEAT);
    }

    void updateGUI() override
    {
        Application::updateGUI();

        ImGui::SetNextWindowPos(ImVec2(0, 0), ImGuiSetCond_FirstUseEver);
        if (ImGui::Begin("MIPT OpenGL Sample", NULL, ImGuiWindowFlags_AlwaysAutoResize))
        {
            ImGui::Text("FPS %.1f", ImGui::GetIO().Framerate);

            if (ImGui::CollapsingHeader("Light"))
            {
                ImGui::ColorEdit3("ambient", glm::value_ptr(_light.ambient));
                ImGui::ColorEdit3("diffuse", glm::value_ptr(_light.diffuse));
                ImGui::ColorEdit3("specular", glm::value_ptr(_light.specular));

                ImGui::SliderFloat("radius", &_lr, 0.1f, 10.0f);
                ImGui::SliderFloat("phi", &_phi, 0.0f, 2.0f * glm::pi<float>());
                ImGui::SliderFloat("theta", &_theta, 0.0f, glm::pi<float>());
            }
            if (ImGui::CollapsingHeader("Detalization"))
            {
				ImGui::SliderInt("polygons", &_polygons, 1, 1000);
				if (_polygons != _last_polygons) {
					_pseudosphere = makePseudosphere(0.5, _polygons);
					_pseudosphere->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));
					_last_polygons = _polygons;
				}
			}
        }
        ImGui::End();
    }

    void draw() override
    {
        //Получаем текущие размеры экрана и выставлям вьюпорт
        int width, height;
        glfwGetFramebufferSize(_window, &width, &height);

        glViewport(0, 0, width, height);

        //Очищаем буферы цвета и глубины от результатов рендеринга предыдущего кадра
		glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

        //====== РИСУЕМ ОСНОВНЫЕ ОБЪЕКТЫ СЦЕНЫ ======
        _commonShader->use();

        //Загружаем на видеокарту значения юниформ-переменных
        _commonShader->setMat4Uniform("viewMatrix", _camera.viewMatrix);
        _commonShader->setMat4Uniform("projectionMatrix", _camera.projMatrix);

        _light.position = glm::vec3(glm::cos(_phi) * glm::cos(_theta), glm::sin(_phi) * glm::cos(_theta), glm::sin(_theta)) * _lr;
        glm::vec3 lightPosCamSpace = glm::vec3(_camera.viewMatrix * glm::vec4(_light.position, 1.0));

		_commonShader->setVec3Uniform("light.pos", lightPosCamSpace); //копируем положение уже в системе виртуальной камеры
        _commonShader->setVec3Uniform("light.La", _light.ambient);
        _commonShader->setVec3Uniform("light.Ld", _light.diffuse);
        _commonShader->setVec3Uniform("light.Ls", _light.specular);

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _sampler);
        _grassTex->bind();
        _commonShader->setIntUniform("diffuseTex", 0);

        //Загружаем на видеокарту матрицы модели мешей и запускаем отрисовку
        {
			if (_polygons != _last_polygons) {
				_pseudosphere = makePseudosphere(0.5, _polygons);
				_pseudosphere->setModelMatrix(glm::translate(glm::mat4(1.0f), glm::vec3(0.0f, 0.0f, 0.5f)));
				_last_polygons = _polygons;
			}
			
            _commonShader->setMat4Uniform("modelMatrix", _pseudosphere->modelMatrix());
            _commonShader->setMat3Uniform("normalToCameraMatrix", glm::transpose(glm::inverse(glm::mat3(_camera.viewMatrix * _pseudosphere->modelMatrix()))));

            _pseudosphere->draw();
        }

        glActiveTexture(GL_TEXTURE0);  //текстурный юнит 0
        glBindSampler(0, _sampler);
        _brickTex->bind();
        _commonShader->setIntUniform("diffuseTex", 0);

        glEnable(GL_STENCIL_TEST);

        glStencilFunc(GL_ALWAYS, 1, 1);
        glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

        glDisable(GL_STENCIL_TEST);

        //Отсоединяем сэмплер и шейдерную программу
        glBindSampler(0, 0);
        glUseProgram(0);
    }
    
    void handleKey(int key, int scancode, int action, int mods) override {
		Application::handleKey(key, scancode, action, mods);
		
		if (action == GLFW_PRESS) {
			if (key == GLFW_KEY_KP_ADD || key == GLFW_KEY_EQUAL) {
				if (_polygons < 7) {
					_polygons = _polygons + 1;
				}	
				if (_polygons < 25 && _polygons >= 7) {
					_polygons = std::min<int>(25, _polygons + 5);
				}
				else if (_polygons >= 25) {
					_polygons = std::min<int>(1000, _polygons + 25);
				}
				
			}
			if (key == GLFW_KEY_MINUS) {
				if (_polygons < 7) {
					_polygons = std::max<int>(1, _polygons - 1);
				}	
				if (_polygons < 25 && _polygons >= 7) {
					_polygons = std::max<int>(6, _polygons - 5);
				}
				else if (_polygons >= 25) {
					_polygons = std::max<int>(24, _polygons - 25);
				}
			}
		}

		//_cameraMover->handleKey(_window, key, scancode, action, mods);
	};
};

int main()
{
    SampleApplication app;
    app.start();

    return 0;
}
